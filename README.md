# add_local_server

Script qui permet d'ajouter un nouveau server local sur votre machine sans avoir besoin de vous rendre dans les fichiers de conf.

Il permet de rajouter l'ip et l'adresse dans l'`httpd` ainsi que de rajouter ces infos dans le `Virtual host`

Il demandera `l'adresse de votre site`, `L'ip de votre site (par defaut 127.0.0.1)` et le dossier où se trouve votre site.

Pour lancer le script il vous suffit de faire `sh add_local_server` !